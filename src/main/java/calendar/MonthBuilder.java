package calendar;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.temporal.TemporalAdjusters;

public class MonthBuilder {
    private int monthNumeric;
    private int yearNumeric;

    private MonthBuilder(int month, int year) {
        monthNumeric = month;
        yearNumeric = year;
    }

    public static MonthBuilder create(int month, int year) {
        return new MonthBuilder(month, year);
    }

    public void display() {
        LocalDate ld = LocalDate.of(yearNumeric, monthNumeric, 1);

        this.printHeader(ld);
        this.printDaysInitials(ld);
        this.printDays(ld);
    }

    private void printHeader(LocalDate ld) {
        String header = String.format("%s %d", ld.getMonth(), ld.getYear());
        // 21 - 7 days of 3 spaces each - the width of the calendar
        int spacing = (21 + header.length()) / 2;
        System.out.println(String.format("%1$"+spacing+"s", header));
    }

    private void printDaysInitials(LocalDate ld) {
        String[] daysInitials = {"Mo", "Tu", "We", "Th", "Fr", "Sa", "Su"};
        for (String di : daysInitials) {
            System.out.print(String.format("%1$3s", di));
        }
        System.out.println();
    }

    private void printDays(LocalDate ld) {
        DayOfWeek dayOfWeek = ld.getDayOfWeek();
        int skippedPlaces = dayOfWeek.getValue() - 1;

        LocalDate lastDay = ld.with(TemporalAdjusters.lastDayOfMonth());
        int lastDayinMonth = lastDay.getDayOfMonth();

        int delta = 7 - skippedPlaces;

        String firstLine = "   ".repeat(skippedPlaces);
        for (int day = 1; day <= delta; day++ ) {
            firstLine += String.format("%1$3s", day);
        }
        System.out.println(firstLine);

        String daysLine = "";

        for (int position = 1; position <= lastDayinMonth - delta; position++) {
            daysLine += String.format("%1$3s", position + delta);
            if (position % 7 == 0) {
                System.out.println(daysLine);
                daysLine = "";
            }
        }
        System.out.println(daysLine); // last line, left over from for-loop
    }
}
