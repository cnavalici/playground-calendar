package calendar;

import java.time.LocalDate;

public class YearBuilder {
    private int yearNumeric;

    public YearBuilder(int year) {
        yearNumeric = year;
    }

    public void display() {
        LocalDate ld = LocalDate.of(yearNumeric, 1, 1);

        this.printHeader(ld);
        this.printMonths(ld);
    }

    private void printHeader(LocalDate ld) {
        String header = String.format("%d", ld.getYear());
        // 21 - 2 - 21 - 2 - 21 - the width of the calendar
        int spacing = (67 + header.length()) / 2;
        System.out.println(String.format("%1$"+spacing+"s", header));
    }

    private void printMonths(LocalDate ld) {
        for (int month=1; month <= 12; month++) {
            MonthBuilder monthBuilder = MonthBuilder.create(month, ld.getYear());
            monthBuilder.display();
            System.out.println();
        }
    }
}
